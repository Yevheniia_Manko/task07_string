package com.manko.task07.part01.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceUtils {

    public boolean checkSentence(String sentence) {
        String sentencePattern = "^[A-Z](.[^.])*\\.";
        Pattern pattern = Pattern.compile(sentencePattern);
        Matcher matcher = pattern.matcher(sentence);
        return matcher.matches();
    }

    public String[] splitSentence(String sentence) {
        String splitPattern = "the|you";
        String[] strings = sentence.split(splitPattern);
        return strings;
    }

    public String replaceVowels (String text) {
        String newText = text.replaceAll("[AEIOUaeiou]", "_");
        return newText;
    }
}
