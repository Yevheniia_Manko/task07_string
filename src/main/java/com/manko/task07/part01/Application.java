package com.manko.task07.part01;

import com.manko.task07.part01.view.ConsoleView;

public class Application {

    public static void main(String[] args) {
        ConsoleView consoleView = new ConsoleView();
        consoleView.show();
    }
}
