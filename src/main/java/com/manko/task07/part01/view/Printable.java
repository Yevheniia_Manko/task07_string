package com.manko.task07.part01.view;

@FunctionalInterface
public interface Printable {

  void print();
}
