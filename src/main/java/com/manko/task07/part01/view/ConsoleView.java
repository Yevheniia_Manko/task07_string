package com.manko.task07.part01.view;

import com.manko.task07.part01.controller.SentenceController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ConsoleView {

    private SentenceController controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private ResourceBundle bundle;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() {
        bundle = ResourceBundle.getBundle("MyMenu", Locale.getDefault());
        controller = new SentenceController();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Check the sentence");
        menu.put("2", "  2 - Split the sentence on the word \"the\" or \"you\"");
        menu.put("3", "  3 - Replace all vowels with underscores");
        menu.put("4", "  4 - Change menu language to Spanish");
        menu.put("5", "  5 - Change menu language to Ukrainian");
        menu.put("6", "  6 - Change menu language to Hindi");
        menu.put("7", "  7 - Change menu language to Persian");
        menu.put("8", "  8 - Change menu language to English");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printCheckingResult);
        methodsMenu.put("2", this::printSentenceParts);
        methodsMenu.put("3", this::showReplacingResult);
        methodsMenu.put("4", this::showSpanishMenu);
        methodsMenu.put("5", this::showUkrainianMenu);
        methodsMenu.put("6", this::showHindiMMenu);
        methodsMenu.put("7", this::showPersianMMenu);
        methodsMenu.put("8", this::showEnglishMMenu);
        methodsMenu.put("Q", this::exit);
    }

    private void printCheckingResult() {
        String userInput = readUserInput();
        boolean result = controller.getCheckingResult(userInput);
        if (result) {
            logger.info("Correct!");
        } else {
            logger.info("Incorrect!");
        }
    }

    private void printSentenceParts() {
        String userInput = readUserInput();
        String[] stringArray = controller.getSentenceParts(userInput);
        Arrays.stream(stringArray).forEach(s -> logger.info(s));
    }

    private void showReplacingResult() {
        String userInput = readUserInput();
        String result = controller.getReplacingResult(userInput);
        logger.info("The resulting string:");
        logger.info(result);
    }

    private String readUserInput() {
        logger.info("Please enter the sentence:");
        String userInput = input.nextLine();
        return userInput;
    }

    private void exit() {

    }

    private void showSpanishMenu() {
        bundle = ResourceBundle.getBundle("MyMenu", Locale.forLanguageTag("es"));
    }

    private void showUkrainianMenu() {
        bundle = ResourceBundle.getBundle("MyMenu", Locale.forLanguageTag("uk"));
    }

    private void showHindiMMenu() {
        bundle = ResourceBundle.getBundle("MyMenu", Locale.forLanguageTag("hi"));
    }

    private void showPersianMMenu() {
        bundle = ResourceBundle.getBundle("MyMenu", Locale.forLanguageTag("fa"));
    }

    private void showEnglishMMenu() {
        bundle = ResourceBundle.getBundle("MyMenu", Locale.forLanguageTag("en"));
    }

    private void outputMenu() {
        logger.info(bundle.getString("title"));
        for (Map.Entry<String, String> pair : menu.entrySet()) {
            logger.info(bundle.getString(pair.getKey()));
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info(bundle.getString("choice"));
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info(bundle.getString("errorItem"));
            }
        } while (!keyMenu.equals("Q"));
    }
}
