package com.manko.task07.part01.controller;

import com.manko.task07.part01.model.SentenceUtils;

public class SentenceController {

    private SentenceUtils sentenceUtils;

    public SentenceController() {
        sentenceUtils = new SentenceUtils();
    }

    public boolean getCheckingResult(String sentence) {
        if (sentence.isEmpty()) {
            return false;
        }
        return sentenceUtils.checkSentence(sentence);
    }

    public String[] getSentenceParts(String userInput) {
        if (userInput.isEmpty() || userInput.equals(" ")) {
            return new String[0];
        }
        return sentenceUtils.splitSentence(userInput);
    }

    public String getReplacingResult(String userInput) {
        if (userInput.isEmpty() || userInput.equals(" ")) {
            return "";
        }
        return sentenceUtils.replaceVowels(userInput);
    }
}
