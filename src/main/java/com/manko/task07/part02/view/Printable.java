package com.manko.task07.part02.view;

@FunctionalInterface
public interface Printable {

  void print();
}
