package com.manko.task07.part02.view;

import com.manko.task07.part01.view.Printable;
import com.manko.task07.part02.controller.TextController;
import com.manko.task07.part02.model.Sentence;
import com.manko.task07.part02.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

public class UserConsoleView {

    private TextController controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner scanner;
    private static Logger logger = LogManager.getLogger(UserConsoleView.class);

    public UserConsoleView() {
        controller = new TextController();
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Print the separate sentences of the text.");
        menu.put("2", "  2 - Print each word and its quantities in each sentence.");
        menu.put("3", "  3 - Show the number of sentences with repeating words.");
        menu.put("4", "  4 - Print sentences in the order of increasing word quantity.");
        menu.put("5", "  5 - Print all questions.");
        menu.put("6", "  6 - Print the words with defined length from all question sentences.");
        menu.put("7", "  7 - Print the longest word in each sentence.");
        menu.put("8", "  8 - Print the first word that starts with a vowel in each sentence.");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printSeparateSentences);
        methodsMenu.put("2", this::printWordQuantity);
        methodsMenu.put("3", this::printNumberSentencesWithRepeats);
        methodsMenu.put("4", this::printSentencesIncreasingWordCountOrder);
        methodsMenu.put("5", this::printAllQuestions);
        methodsMenu.put("6", this::printDefinedLengthWordsFromQuestions);
        methodsMenu.put("7", this::printLongestWord);
        methodsMenu.put("8", this::printFirstWordStartsWithVowel);
        methodsMenu.put("Q", this::exit);
    }

    public void show() {
        readFileName();
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, input menu item:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Incorrect menu item!");
            }
        } while (!keyMenu.equals("Q"));
    }

    private void readFileName() {
        logger.info("Input file name: ");
        String userInput = scanner.nextLine();
        try {
            controller.readTextFromFile(userInput);
        } catch (IOException e) {
            logger.info("File not found");
            readFileName();
        }
    }

    private void outputMenu() {
        logger.info("\nMENU");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    private void printSeparateSentences() {
        List<Sentence> list = controller.showAllSentences();
        list.forEach(logger::info);
    }


    private void printWordQuantity() {
        List<Sentence> list = controller.showAllSentences();
        for (Sentence sentence : list) {
            Map<Word, Integer> map = sentence.getWordCountMap();
            for (Map.Entry<Word, Integer> pair : map.entrySet()) {
                logger.info(pair.getKey() + " : " + pair.getValue());
            }
            logger.info("");
        }
    }

    private void printNumberSentencesWithRepeats() {
        int count = controller.showNumberSentencesWithRepeats();
        if (count == 0) {
            logger.info("No repeating words in this text sentences.");
        } else {
            logger.info("The number of sentences with repeating words is " + count);
        }
    }

    private void printSentencesIncreasingWordCountOrder() {
        Map<Integer, String> map = controller.showSortedList();
        for (Map.Entry<Integer, String> pair: map.entrySet()) {
            logger.info(pair.getKey() + " " + pair.getValue());
        }
    }

    private void printAllQuestions() {
        List<Sentence> questions = controller.showAllQuestions();
        if (questions.isEmpty()) {
            logger.info("There is no question in this text.");
        } else {
            questions.forEach(logger::info);
        }
    }

    private void printDefinedLengthWordsFromQuestions() {
        logger.info("Please input the word length:");
        String wordLength = scanner.nextLine();
        try {
            Set<Word> words = controller.getWordDefinedLengthSet(wordLength);
            if (words.isEmpty()) {
                logger.info("There is no word with length " + wordLength + " in question sentences of this text.");
            } else {
                words.forEach(logger::info);
            }
        } catch (NumberFormatException ex) {
            logger.info("Please enter the length of the words.");
        }
    }

    private void printLongestWord() {
        List<Word> longestWords = controller.showLongestWords();
        logger.info("The longest words in each sentence of the text:");
        longestWords.forEach(logger::info);
    }

    private void printFirstWordStartsWithVowel() {
        List<String> list = controller.showFirstFoundedWordsStartsVowel();
        if (list.isEmpty()) {
            logger.info("There is no word that starts with vowel in this text.");
        } else {
            list.forEach(logger::info);
        }
    }

    private void exit() {
        logger.info("You finished the application work.");
    }
}
