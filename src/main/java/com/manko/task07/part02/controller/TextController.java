package com.manko.task07.part02.controller;

import com.manko.task07.part02.model.Sentence;
import com.manko.task07.part02.model.TextManager;
import com.manko.task07.part02.model.Word;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class TextController {

    private TextManager textManager;

    public TextController() {
        textManager = new TextManager();
    }

    public void readTextFromFile(String fileName) throws IOException {
        StringBuilder builder = new StringBuilder("");
        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNext()) {
                builder.append(scanner.nextLine()).append(" ");
            }
        }
        String s = new String(builder);
        textManager.setText(s);
    }

    public List<Sentence> showAllSentences() {
        return textManager.getSentences();
    }

    public int showNumberSentencesWithRepeats() {
        return textManager.getNumberSentencesWithRepeatingWords();
    }

    public Map<Integer, String> showSortedList() {
        return textManager.getSortedList();
    }

    public List<Sentence> showAllQuestions() {
        return textManager.getAllQQuestions();
    }

    public Set<Word> getWordDefinedLengthSet(String s) throws NumberFormatException {
        int wordLength = Integer.parseInt(s);
        return textManager.getWordsDefinedLengthFromQuestions(wordLength);
    }

    public List<Word> showLongestWords() {
        return textManager.getLongestWords();
    }

    public List<String> showFirstFoundedWordsStartsVowel() {
        return textManager.getFirstWordsStartsWithVowels();
    }
}
