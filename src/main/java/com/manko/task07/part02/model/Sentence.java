package com.manko.task07.part02.model;

import java.util.*;
import java.util.stream.Collectors;

public class Sentence implements Comparator<Sentence> {

    private String textSentence;
    private Map<Word, Integer> wordCountMap = new TreeMap<>();
    private int countWords;
    private Word firstWord;
    private SentenceType sentenceType;

    public Sentence(String textSentence) {
        this.textSentence = textSentence;
        this.wordCountMap = createWordCountMap();
        this.countWords = calculateWordsCount();
        this.sentenceType = checkSentenceType();
    }

    public String getTextSentence() {
        return textSentence;
    }

    public Map<Word, Integer> getWordCountMap() {
        return wordCountMap;
    }

    public int getCountWords() {
        return countWords;
    }

    public Word getFirstWord() {
        return firstWord;
    }

    public SentenceType getSentenceType() {
        return sentenceType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sentence sentence = (Sentence) o;

        if (countWords != sentence.countWords) return false;
        return textSentence != null ? textSentence.equals(sentence.textSentence) : sentence.textSentence == null;
    }

    @Override
    public int hashCode() {
        int result = textSentence != null ? textSentence.hashCode() : 0;
        result = 31 * result + countWords;
        return result;
    }

    @Override
    public String toString() {
        return textSentence;
    }

    @Override
    public int compare(Sentence s1, Sentence s2) {
        int result = s1.countWords - s2.countWords;
        if (result != 0) {
            return result;
        }
        return s1.firstWord.compareTo(s2.firstWord);
    }

    private Map<Word, Integer> createWordCountMap() {
        // replace all symbols except words, spaces and apostrophe, then split the string on the spaces
        String[] words = textSentence.replaceAll("[^\\w\\s’]", "").replaceAll("\\s\\s[\\s]*", " ").split(" ");
        List<Word> wordList = Arrays.stream(words).map(s -> new Word(s)).collect(Collectors.toList());
        for (Word word : wordList) {
            if (wordCountMap.containsKey(word)) {
                int count = wordCountMap.get(word) + 1;
                wordCountMap.replace(word, count);
            } else {
                wordCountMap.put(word, 1);
            }
        }
        return wordCountMap;
    }

    private int calculateWordsCount() {
        int count = 0;
        for (Map.Entry<Word, Integer> map : wordCountMap.entrySet()) {
            count +=map.getValue();
        }
        return count;
    }

    private SentenceType checkSentenceType() {
        if (textSentence.endsWith(".")) {
            sentenceType = SentenceType.NARRATIVE;
        }
        if (textSentence.endsWith("!")) {
            sentenceType = SentenceType.EXCLAMATION;
        }
        if (textSentence.endsWith("?")) {
            sentenceType = SentenceType.QUESTION;
        }
        return sentenceType;
    }
}
