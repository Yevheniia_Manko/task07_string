package com.manko.task07.part02.model;

public enum SentenceType {
    NARRATIVE, QUESTION, EXCLAMATION
}
