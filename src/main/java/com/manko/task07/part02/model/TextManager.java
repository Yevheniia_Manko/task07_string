package com.manko.task07.part02.model;

import java.util.*;
import java.util.stream.Collectors;

public class TextManager {

    private String text;
    private List<Sentence> sentences;

    public TextManager() {
        sentences = new ArrayList<>();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = removeDoubleWhitespaces(text);
        sentences = splitTextOnSentences(this.text);
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public int getNumberSentencesWithRepeatingWords() {
        int count = 0;
        for (Sentence sentence : sentences) {
            Map<Word, Integer> map = sentence.getWordCountMap();
            for (Map.Entry<Word, Integer> pair : map.entrySet()) {
                if (pair.getValue() > 1) {
                    count++;
                }
            }
        }
        return count;
    }

    public Map<Integer, String> getSortedList() {
        Map<Integer, String> map = new TreeMap<>();
        for (Sentence sentence : sentences) {
            map.put(sentence.getCountWords(), sentence.getTextSentence());
        }
        return map;
    }

    public List<Sentence> getAllQQuestions() {
        List<Sentence> questions = sentences.stream()
                .filter(s -> s.getSentenceType() == SentenceType.QUESTION).
                collect(Collectors.toList());
        return questions;
    }

    public Set<Word> getWordsDefinedLengthFromQuestions(int wordLength) {
        List<Sentence> questions = getAllQQuestions();
        Set<Word> wordsDefinedLength = new TreeSet<>();
        for (Sentence sentence : questions) {
            Set<Word> words = sentence.getWordCountMap().keySet();
            for (Word word : words) {
                if (word.getWord().length() == wordLength) {
                    wordsDefinedLength.add(word);
                }
            }
        }
        return wordsDefinedLength;
    }

    public List<Word> getLongestWords() {
        return sentences.stream().map(s -> findLongestWord(s)).collect(Collectors.toList());
    }

    public List<String> getFirstWordsStartsWithVowels() {
        return sentences.stream().map(s -> findFirstWordStartsWithVowel(s)).collect(Collectors.toList());
    }

    private String findFirstWordStartsWithVowel(Sentence sentence) {
        String[] words = sentence.getTextSentence().replaceAll("[.,!?]", "").split(" ");
        String vowels = "AEIOUaeiou";
        String foundedWord = "";
        Optional<String> word = Arrays.stream(words)
                .filter(s -> vowels.contains(s.substring(0, 1)))
                .findFirst();
        if (word.isPresent()) {
            foundedWord = word.get();
        }
        return foundedWord;
    }

    private Word findLongestWord(Sentence sentence) {
        Map<Word, Integer> map = sentence.getWordCountMap();
        Set<Word> words = map.keySet();
        Word longestWord = new Word("");
        int length = 0;
        for (Word word : words) {
            if (word.getWord().length() > length) {
                length = word.getWord().length();
                longestWord = word;
            }
        }
        return longestWord;
    }

    private String removeDoubleWhitespaces(String str) {
        String newStr = str.replaceAll("\\s\\s[\\s]*|\\t|\\n", " ").trim();
        return newStr;
    }

    private List<Sentence> splitTextOnSentences(String someText) {
        String[] textSentences = someText.replace(". ", ".\n").replace("! ", "!\n").replace("? ", "?\n").split("\n");
        sentences = Arrays.stream(textSentences).map(s -> new Sentence(s)).collect(Collectors.toList());
        return sentences;
    }
}
