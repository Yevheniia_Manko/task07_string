package com.manko.task07.part02;

import com.manko.task07.part02.view.UserConsoleView;

public class Appl {

    public static void main(String[] args) {
        UserConsoleView view = new UserConsoleView();
        view.show();
    }
}
